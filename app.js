import Map from 'ol/Map.js';
import View from 'ol/View.js';
import {defaults as defaultControls} from 'ol/control.js';
import MousePosition from 'ol/control/MousePosition.js';
import {createStringXY} from 'ol/coordinate.js';
import TileLayer from 'ol/layer/Tile.js';
import OSM from 'ol/source/OSM.js';
import coordinate from 'ol/coordinate.js';
import BingMaps from 'ol/source/BingMaps.js';

import {fromLonLat} from 'ol/proj.js';
import Projection from 'ol/proj/Projection.js';

import TileWMS from 'ol/source/TileWMS.js';
import ImageWMS from 'ol/source/ImageWMS.js';
import Image from 'ol/layer/Image.js'; // for displaying the imagewms as an image
import { deepStrictEqual } from 'assert';

let layersToAnimate = []; // the array to be filled with the layers to animate in animateWMS

var bingKey = 'AtD5lB1W0i2OeWMymtd8vjGYKwbo0hiiztm16QNmXoB2E5bucHFh5MvCb4mtwueG';

const regeneratorRuntime = require("regenerator-runtime"); // require browser to have, for async functions

// bing maps styles to be used when selecting a base layer
var mapStyles = [
    'Road',
    'Aerial'
];

var map; // the map that will be modified throughout the program

// a new control to add to map controlls. map defaults to displaying mouse coordinates when this is added to controls in map
// displays on the coord-txt div element
var mousePositionControl = new MousePosition({
    coordinateFormat: createStringXY(4),
    projection: 'EPSG:4326',
    className: 'custom-mouse-position',
    target: document.getElementById('coord-txt'),
    undefinedHTML: '&nbsp;'
  });

  // the background map upon which everything is displayed
  function baseMap() {
    map = new Map({
        controls: defaultControls().extend([mousePositionControl]), // adds mouse position coordinate as default
        layers: [
        new TileLayer({
            source: new OSM()
        })
        ],
        target: 'map',
        view: new View({
        center: fromLonLat([-79.95, 39.62]),
        zoom: 4
        })
    });
  }


// displays the date and time in the date div
function setDate() {
    const monthNames = ["January", "February", "March", "April", "May", "June",
     "July", "August", "September", "October", "November", "December"];
    var today = new Date();
    var date = today.getUTCDate() + ' ' + monthNames[today.getMonth()] + ' ' + today.getUTCFullYear() + ' ' + today.getUTCHours() + ':' + today.getUTCMinutes() + ' UTC';
    document.getElementById('date-txt').innerHTML = date;

    setTimeout(setDate, 1000); // update time every 1000ms
}

// holds the base maps and added when a new map is chosen in layers
var layers = [];
var i, ii;
for (i = 0, ii = mapStyles.length; i < ii; i++) {
    layers.push(new TileLayer({
        visible: true,
        preload: Infinity,
        source: new BingMaps({
            key: bingKey,
            imagerySet: mapStyles[i]
        })
    }));
}


// url components for querying for displayed weather info
var zipcode;
var weatherUrl;
var baseWeatherUrl = "https://api.openweathermap.org/data/2.5/weather?zip=";
var endWeatherUrl = "&units=imperial&appid=df2de3900e635249f5651233e62fd47c";

// used with addWeatherInfo function
var weatherData;
var city;
var tempData;
var windDir;   
var windSpeed;
var humidity;
var lon;
var lat;

// function to query openweathermap for weather data
// sends query to openweathermap and then displays the returned info
function addWeatherInfo() {
    // get the zip
    weatherUrl = baseWeatherUrl + document.getElementById("zip-txt").value + endWeatherUrl;
    document.getElementById("weather-box").style.display = "block"; 
    // makes a query and waits for it to be returned before continuing (async:false) and fills variables with the data
    $.ajax({
      url: weatherUrl,
      dataType: 'json',
      async: false,
      success: function(data) {
        weatherData = data;
        city = weatherData.name;
        tempData = weatherData.main.temp;
        windDir = weatherData.wind.deg;
        windSpeed = weatherData.wind.speed;
        humidity = weatherData.main.humidity;
        lon = weatherData.coord.lon;
        lat = weatherData.coord.lat;
      }
    })
  
    // set html elements to the returned info
    if (weatherData) {
      document.getElementById('wind-direction').innerHTML = "Wind Direction: " + windDir + "°";
      document.getElementById('temperature').innerHTML = "Temp: " + tempData + '°' + 'F';
      document.getElementById('wind-speed').innerHTML = "Wind Speed: " + windSpeed + "fps";
      document.getElementById('humidity').innerHTML = "Humidity: " + humidity + "%";
      document.getElementById('location-txt').innerHTML = "Mockup Project, " + city;
      map.getView().setCenter(fromLonLat([lon, lat])); // sets the center of the map to the user entered zip code(lon/lat)
    }
  }


// adds the selected wms image layer to the map. 
function addWMSLayer(url, layer) { // url to wmsServer, weather image

    popLayer(); // pops topmost layer off map if it exists

    var source = new ImageWMS({
        url: url,
        params: {'LAYERS': layer} // layer selected precipitation/wind
    })
    
    var wmsLayer = new Image({
        title: 'NOAA',
        zIndex: 1,
        visible: true,
        source: source,
        opacity: 0.7
    });

    map.addLayer(wmsLayer);
}

// loads multiple wind wms layers for use with animation and returns an array of them
function getMultiWind() {
    let wmsLayers = []; // will be filled with the wind wms layers
    var layersToAdd = ['45', '41', '37', '33', '29', '25', '21', '17', '13', '9', '5', '1']; // the layers to call from the nowcoast server +6hrs each time

    for (var i = 0; i < layersToAdd.length; i++) { // add each of the layersToAdd layers to wmsLayers
        var source = new ImageWMS({
            url: "https://nowcoast.noaa.gov/arcgis/services/nowcoast/forecast_meteoceanhydro_sfc_ndfd_windspeed_offsets/MapServer/WMSServer?",
            params: {LAYERS: layersToAdd[i]}
        })

        var windLayer = new Image({
            title: 'NOAA',
            zIndex: 1,
            visible: false,
            source: source,
            opacity: 0.7
        })

        wmsLayers[i] = windLayer;
    }

    return wmsLayers;
}

// gets the precipitation wms layers from noaa
function getMultiPrecip() {
    var wmsLayers = [];
    // +1hr, +3hr, +6hr, 12, 24, 48, 72
    var layersToAdd = ['25', '21', '17', '13', '9', '5', '1'];

    for (var i = 0; i < layersToAdd.length; i++) { // add each of the layersToAdd layers to wmsLayers
        var source = new ImageWMS({
            url: "https://nowcoast.noaa.gov/arcgis/services/nowcoast/analysis_meteohydro_sfc_qpe_time/MapServer/WMSServer?",
            params: {LAYERS: layersToAdd[i]}
        })

        var precipLayer = new Image({
            title: 'NOAA',
            zIndex: 1,
            visible: false,
            source: source,
            opacity: 0.7
        })

        wmsLayers[i] = precipLayer;
    }

    return wmsLayers;
}

// takes an array of layers to display and iterates through them, displaying each on the map
let waitTime = 1000; // 1000ms, the time to wait between animation frames
let pause = 0; // if set to 1, pause the animation
let loop = 0; // if set to 1, continuously loop through the animation
let endAnimation = 0; // if 1, break from animation
async function animateWMS() {
    popAllLayers();
    console.log('entered animateWMS');

    var filled = 0; // check if the map has been filled with the invisible layers for animation

    // add all the layers to the map (initially invisible)
        for (var i = 0; i < layersToAnimate.length; i++) {
            console.log("inside layer add");
            map.addLayer(layersToAnimate[i]);
            console.log("added layer " + i);         
        }
            
    // set each layer to visible then back to invisible
        for (var indx = 0; indx <= layersToAnimate.length; indx++){

            if (pause === 0) { // continue if pause is not selected 
            
                
                // get the value for the loop checkbox for use in the animation loop
                var checked = document.getElementById("loop").checked;
                if (checked) loop = 1;
                if (!checked) loop = 0;

                layersToAnimate[indx].setVisible(true); // set the current layer to visible
                
                await sleep(waitTime); // pause between frames of the animation
                console.log("waitTime = : " + waitTime);
                console.log(indx);

                // check for pause
                if (pause === 1) {
                    console.log("paused");
                    while (pause === 1) {
                        await sleep(2000);
                    }
                    console.log("unpaused");
                }

                layersToAnimate[indx].setVisible(false); // set the current layer to invisible     
                
                if (loop === 1) { // if loop is selected, keep looping through animation
                    if (indx >= layersToAnimate.length-1) {
                        indx = -1;
                    }
                }

            }
            if (endAnimation === 1) break; // exits animation when user closes animation utitlites panel
        }
        console.log("exit animateWMS");
}

// function to delay the program when called, used in async functions
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// removes current layer and adds a bing map layer from layers
function addBingMap(mapLayer) {
    map.getLayers().pop();
    map.addLayer(layers[mapLayer]);
}

// removes current layer and adds the default osm layer on click
function addDefaultMap() {
    var defaultLayer = new TileLayer({
        source: new OSM()
    });
    map.getLayers().pop();
    map.addLayer(defaultLayer);
}

// pops the top layer as long as it's not the base layer
function popLayer() {
    if (map.getLayers().getLength() > 1) {
        map.getLayers().pop();
    }
}

// pops all layers but not the base layer
function popAllLayers() {
    for (var i = 0; i < map.getLayers().getLength(); i++) {
        if (map.getLayers().getLength() > 1) {
            map.getLayers().pop();
        }
    }
}

// calls functions and handles real time user interactions
function main() {
    baseMap(); // place the initial base map
    setDate(); // display current time
    document.getElementById("satellite-map").onclick = function() {
        addBingMap(1);
    }

    document.getElementById("road-map").onclick = function() {
        addBingMap(0);
    }

    document.getElementById("default-map").onclick = function() {
        addDefaultMap();
    }

    document.getElementById("precip-24").onclick = function() { // add 24hr precip accumulation wms layer
        addWMSLayer("https://nowcoast.noaa.gov/arcgis/services/nowcoast/analysis_meteohydro_sfc_qpe_time/MapServer/WmsServer?", '9');
        document.getElementById("legend").style.display = "block" // set legend to visible
        // change the legend's image src
        document.getElementById("legend-src").src = "https://nowcoast.noaa.gov/arcgis/services/nowcoast/analysis_meteohydro_sfc_qpe_time/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=9";
    }

    document.getElementById("wind").onclick = function() {
        addWMSLayer("https://nowcoast.noaa.gov/arcgis/services/nowcoast/forecast_meteoceanhydro_sfc_ndfd_windspeed_offsets/MapServer/WMSServer?", '45');
        document.getElementById("legend").style.display = "block";
        // change the legend's image src
        document.getElementById("legend-src").src = "https://nowcoast.noaa.gov/arcgis/services/nowcoast/forecast_meteoceanhydro_sfc_ndfd_windspeed_offsets/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=45";
    }

    document.getElementById("animate").onclick = function() { // brings up the animation panel
        document.getElementById("utilities-panel").style.display = "block";
    }

    document.getElementById("play").onclick = function() { // start the animation when clicked
        endAnimation = 0; // reset the break variable incase it was set prior
        document.getElementById("legend").style.display = "block";
        document.getElementById("legend-src").src = "https://nowcoast.noaa.gov/arcgis/services/nowcoast/forecast_meteoceanhydro_sfc_ndfd_windspeed_offsets/MapServer/WmsServer?request=GetLegendGraphic%26version=1.3.0%26format=image/png%26layer=45";
        layersToAnimate = getMultiWind(); //getMultiPrecip();
        animateWMS(); 
    }

    document.getElementById("pause-resume").onclick = function() { // pause/resume button, resumes or pauses animation
        // change button's html name to reflect what it will do
        if (document.getElementById("pause-resume").innerHTML === "Pause") {
            document.getElementById("pause-resume").innerHTML = "Resume";
        }
        else {
            document.getElementById("pause-resume").innerHTML = "Pause";
        }

        // update variables for the animation loop
        if (pause === 1) pause = 0;
        else if (pause === 0) pause = 1;
    }

    document.getElementById("slow").onclick = function() { // if slower button is pressed, reduced animation speed
        if (waitTime < 10000) { // keep the wait no more than 10s
            waitTime = waitTime + 250;
        }
    }

    document.getElementById("fast").onclick = function() {
        if (waitTime > 250) { // keep wait no less than 250ms
            waitTime = waitTime - 250;
        }
    }

    document.getElementsByClassName("close-weather")[0].onclick = function() {
        document.getElementById("weather-box").style.display = "none";
    }

    document.getElementsByClassName("close")[1].onclick = function() { // close the animation and animation panel
        document.getElementById("utilities-panel").style.display = "none";
        endAnimation = 1; // break from the animation loop
        popAllLayers();
    }

    document.getElementById("button3").onclick = function() {
        document.getElementById("zip-box").style.display = "block";
    }

    document.getElementById("zip-button").onclick = function() {
        document.getElementById("zip-box").style.display = "none";
        addWeatherInfo();
    }

    document.getElementById("button4").onclick = function() { // when the wms button is clicked, set the wms-modal div's display
        document.getElementById("wms-modal").style.display = "block";
    }

    document.getElementById("layers-button").onclick = function() { // when layers button is clicked, set layers-modal div's display
        document.getElementById("layers-modal").style.display = "block";
    }

    document.getElementsByClassName("close")[0].onclick = function() { // for closing the weather layer's legend
        document.getElementById("legend").style.display = "none";
    }

    document.getElementById("clear").onclick = function() { // clears top layer when clear is clicked
        popAllLayers();
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == document.getElementById("layers-modal")) {
            document.getElementById("layers-modal").style.display = "none";
        }

        if (event.target == document.getElementById("wms-modal")) {
            document.getElementById("wms-modal").style.display = "none";
        }
    }

}

main();


// example of noaa getcapabilities call
//https://nowcoast.noaa.gov/arcgis/services/nowcoast/analysis_meteohydro_sfc_qpe_time/MapServer/WMSServer?request=GetCapabilities&service=WMS

